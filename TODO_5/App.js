import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  const yourname = 'Karma Yangzom';
  return (
    <View style={styles.container}>
      <Text style = {styles.textStyle1}>Getting started with react!</Text>
      <Text style = {styles.textStyle2}> My name is {yourname}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textStyle1: {
    fontSize: 45
  },
  textStyle2: {
    fontSize: 20
  }
});
