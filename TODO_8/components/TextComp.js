import React from "react";
import {Text, StyleSheet, View} from 'react-native';

const TextComp = () =>{
    return (
        <View style= {styles.container}>
            <Text style={styles.text}>The <b>quick brown fox</b> jumps over the lazy dog</Text>
        </View>
    );
}
export default TextComp;
const styles = StyleSheet.create({
    container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    },
    text: {
      fontSize: 16  
    }
    
})