import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    <View style={styles.conatiner}>
      <View style={styles.square} />
      <View style={styles.square} />
      <View style={styles.square} />
    </View>
  );
}
const styles = StyleSheet.create({
  conatiner: {
    backgroundColor: "#7CA1B4",
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  square: {
    backgroundColor: '#7cb48f',
    height: 100,
    width: 100,
    margin: 4,
  },
});
