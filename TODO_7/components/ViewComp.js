import React from "react";
import { View, StyleSheet } from "react-native";

const ViewComp = () => {
    return (
        <View style={styles.container}>
            <View style={styles.box1}></View>
            <View style={styles.box2}></View>
        </View>
    )
}

export default ViewComp

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent:'center',
        width:100,

      },
    box1: {
        backgroundColor: 'red',
        flex: 0.2,
        
    },
    box2: {
        backgroundColor: 'blue',
        flex:0.2
    }
})

