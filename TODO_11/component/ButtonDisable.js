import React, { useState } from 'react';
import { Button, StyleSheet, Text, View } from 'react-native';

const ButtonDisable = () => {
  const [count, setCount] = useState(1);
  const [text, setText] = useState("The button isn't pressed yet");
  const [disable, setDisable] = useState(false)

  return (
    <View style={styles.container}>
      <Text style={styles.text}>{text}</Text>
        <Button
        title='PRESS ME'
        disabled={disable}
        onPress={()=>{
          if(count<4){
            setText("The button was pressed " + count + " times!")
            setCount(count+1)
            if(count==3){
              setDisable(true)
            }
          }
        }}
        />
    </View>
  )
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    width:300
  },
  text: {
    alignSelf:'center'
  }
});
export default ButtonDisable
