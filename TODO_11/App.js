import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Button } from 'react-native';
import ButtonDisable from './component/ButtonDisable';
export default function App() {
  return (
    <View style={styles.container}>
      <ButtonDisable></ButtonDisable>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
});
