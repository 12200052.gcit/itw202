import { StyleSheet, Text, View } from 'react-native';
import ImageComp from './components/ImageComp';

export default function App() {
  return (
    <View style={styles.container}>
      <ImageComp></ImageComp>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
