import React from "react";
import {View, Image, StyleSheet } from 'react-native';

const ImageComp = () => {
    return (
        <View>
             <Image
            style={styles.image1}
            source={{uri: 'https://picsum.photos/100/100'}}
            />
            <Image
            style={styles.image2}
            source={require('../assets/react_native_logo.png')}
            />
        </View>
    )
}
export default ImageComp

const styles = StyleSheet.create({
    image1: {
        height: 100,
        width:300,
        resizeMode: 'contain',
    },
    image2: {
        height: 100,
        width:300,
        resizeMode: 'contain',
    },
});