import { StyleSheet, Text, View, Image } from 'react-native';
import { Provider } from 'react-native-paper';
import { theme } from './src/core/theme';
import Button from './src/components/Button';
import TextInput from './src/components/TextInput';
import Header from './src/components/Header';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {createDrawerNavigator} from '@react-navigation/drawer';
import 'react-native-gesture-handler'
import firebase from 'firebase/app';
import { 
  LoginScreen, 
  StartScreen,
  RegisterScreen,
  ResetPasswordScreen,
  HomeScreen,
  ProfileScreen,
  AuthloadingScreen
} from './src/Screens/Index';
import DrawerContent from './src/components/DrawerContent';
import { firebaseConfig } from './src/core/config';

if (!firebase.apps.length){
  firebase.initializeApp(firebaseConfig);
}

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

export default function App() {
  return (
    <Provider theme={theme}>
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName='AuthLoadingScreen'
          screenOptions={{headerShown: false}}
          >
          <Stack.Screen name='AuthLoadingScreen' component={AuthloadingScreen}/>
          <Stack.Screen name='StartScreen' component={StartScreen}/>
          <Stack.Screen name='LoginScreen' component={LoginScreen}/>
          <Stack.Screen name='RegisterScreen' component={RegisterScreen}/>
          <Stack.Screen name='ResetPasswordScreen' component={ResetPasswordScreen}/>
          <Stack.Screen name='HomeScreen' component={DrawerNavigator}/>
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>    
    
  );
}

function BottomNavigation(){
  return (
    <Tab.Navigator>
      <Tab.Screen 
      name='Home' 
      component={HomeScreen}
      options = {{
        tabBarIcon: ({size}) => {
          return (
            <Image
            style={{ width: size, height: size}}
            source={
              require('./assets/Homeicon.jpeg')
          }
          />
          )
        }
      }}
      />
      <Tab.Screen 
      name='Profile' 
      component={ProfileScreen}
      options = {{
        tabBarIcon: ({size}) => {
          return (
            <Image
            style={{ width: size, height: size}}
            source={
              require('./assets/settingIcon.jpeg')
          }
          />
          )
        }
      }}
      />
    </Tab.Navigator>
  )
}
const DrawerNavigator = () => {
  return (
       <Drawer.Navigator drawerContent={DrawerContent}>
          <Drawer.Screen name="HomeScreen" component={BottomNavigation} />
        </Drawer.Navigator>     
  );
}

