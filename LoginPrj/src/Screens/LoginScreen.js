import React, {useState} from "react";
import {Text, View, StyleSheet, TouchableOpacity } from "react-native";
import Button from "../components/Button";
import Header from "../components/Header";
import Background from "../components/Background";
import Logo from "../components/Logo";
import TextInput from "../components/TextInput";
import { emailValidator } from "../core/helpers/emailValidator";
import { passwordValidator } from "../core/helpers/PasswordValidator";
import BackButton from "../components/BackButton";
import { theme } from "../core/theme";
import { loginUser } from "../api/auth-api";

export default function LoginScreen({navigation}){
    const [email, setEmail] = useState({value: "", error: ""})
    const [password, setPassword] = useState({value: "", error: ""})
    const [loading, setLoading] = useState();

    const onLoginPressed = async() => {
        const emailError = emailValidator(email.value);
        const passwordError = passwordValidator(password.value);
        if (emailError || passwordError) {
            setEmail({ ...email, error: emailError });
            setPassword({ ...password, error: passwordError });
        }
        setLoading(true)
        const response = await loginUser({
            email: email.value,
            password: password.value
        });
        if (response.error){
            alert(response.error)
        }else {
            navigation.navigate('HomeScreen')
        }
        setLoading(false)

    }
    return (
        <Background>
            <BackButton goBack={navigation.goBack}/>
            <Logo/>
            <Header>Welcome</Header>
            <TextInput 
                label="Email"
                value={email.value}
                error={email.error}
                errorText={email.error}
                onChangeText={(text) => setEmail({ value: text, error: "" })} 
            />
            <TextInput 
                label="Password"
                value={password.value}
                error={password.error}
                onChangeText={(text) => setPassword({ value: text, error: "" })} 
                secureTextEntry
            />
             <View style={styles.forgotPassword}>
                <TouchableOpacity onPress={() => navigation.navigate("ResetPasswordScreen")}>
                <Text style={styles.forgot}>Forgot your Password?? </Text>
                </TouchableOpacity>
            </View>
            <Button loading = {loading} mode="contained" onPress = {onLoginPressed}>Login</Button>
            <View style={styles.row}>
                <Text>Don't have an account? </Text>
                <TouchableOpacity onPress={() => navigation.replace("RegisterScreen")}>
                    <Text style={styles.link}>Sign Up</Text>
                </TouchableOpacity>
            </View>
        </Background>
    )
}

const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        marginTop: 4,
    },
    link: {
        fontWeight: 'bold',
        color: theme.colors.primary,
    },
    forgotPassword: {
        width: '100%',
        alignItems: 'flex-end',
        marginBottom: 24,
    },
    forgot: {
        fontSize: 11,
        color: theme.colors.secondary,
    },
})