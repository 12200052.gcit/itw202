import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import CustomComponents from './components/CustomComponent';
import { RowItem } from './components/RowItem';

export default function App() {
  return (
    <View style={styles.container}>
      <CustomComponents></CustomComponents>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
