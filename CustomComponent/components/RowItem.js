import React from 'react'
import { TouchableOpacity, Text, View, StyleSheet} from 'react-native'
import {COLORS} from '../constants/Colors';

export const RowItem = ({text}) => {
    return (
            <TouchableOpacity style={styles.row}>
                <Text style={styles.text}>{text}</Text>
            </TouchableOpacity >
    )
}
const styles = StyleSheet.create({
    conatiner:{
        marginTop: 30
    },
    row:{
        paddinfHorizontal: 20,
        paddingVertical: 16,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor:COLORS.white,
    },
    text:{
        color: COLORS.text,
        fontSize: 16,
    },
})