import React from 'react'
import { View, StyleSheet} from 'react-native'
import {COLORS} from '../constants/Colors';
import { RowItem } from './RowItem';

function CustomComponents() {
    return (
        <View style={styles.conatiner}>
           <RowItem
           text="Themes"
           />
           <RowItem
           text=" React Native Basics"
           />
           <RowItem
           text="React Native by Example"
           />
        </View>
    )
}
export default CustomComponents
const styles = StyleSheet.create({
    conatiner:{
        marginTop: 30
    },
    row:{
        paddinfHorizontal: 20,
        paddingVertical: 16,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor:COLORS.white,
    },
    title:{
        color: COLORS.text,
        fontSize: 16,
    },
})