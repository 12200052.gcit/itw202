import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, TextInput, Button } from 'react-native';
import styles from './component/styles';
{/*export default function App() {
  return (
    //Inline styles
    <View style={{ flex: 1, justifyContent: 'center',
      alignItems: 'center', backgroundColor: '#F5FCFF'
      }}>
      <Text style = {{ fontSize: 20, textAlign: 
        'center', margin: 10}}>
          Welcome to React Native!
        </Text>
    </View>

  );
}*/}

export default function App() {
  return (
    <View style = { styles.container }>
      <Text style = { styles.welcome}>
          Welcome to React Native!
        </Text>
    </View>

  );
}
{/*const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'F5FCFF'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10
  }
})*/}