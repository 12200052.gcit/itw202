import React, {useState} from "react";
import {  View, Text, StyleSheet, TextInput } from "react-native";

const TextInputComp = () => {
  const [name, setName] = React.useState('');

  return (
    <View>
      <Text style={styles.text}> What is your name?</Text>
      <TextInput
        style={styles.input}
        onChangeText={(value) => setName(value)}
        keyboardType='default'
      />
      <Text style={styles.text }>Hi, {name} from Gyalpozhing College of Information Technology!</Text>
      <TextInput
      style={styles.input}
      placeholder="*****************"
      />
    </View>
  
  );
};

const styles = StyleSheet.create({
  input: {
    height: 40,
    margin: 12,
    borderColor:'silver',
    backgroundColor:'silver',
    padding: 10,
  },
  text: {
    margin:12
  }
});

export default TextInputComp;