import { View, Text, StyleSheet, Platform  } from 'react-native'
import React from 'react'
// import Colors from '../../constants/Colors'

export default function Title({children}) {
  return (
    <View>
      <Text style={styles.title}>{children}</Text>
    </View>
  )
}
const styles=StyleSheet.create({
    title: {
      fontFamily:'open-sans-bold',
        // borderWidth: Platform.OS ==='andriod' ? 2 : 0 ,
        borderWidth: Platform.select({ios:0, android:2}),
        borderColor:'white',
        fontSize:24,
        textAlign:'center',
        // fontWeight:'bold',
        color:'white',
        padding:12,
        maxWidth:'80%',
        width:300,
    }
})
