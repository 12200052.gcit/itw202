import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import Colors from '../../constants/Colors'
import { Dimensions } from 'react-native'

function NumberContainer({children}) {
    return (
        <View style={styles.container}>
            <Text style={styles.numberText}>{children}</Text>
        </View>
    )
  }
  export default NumberContainer
  const deviceWidth = Dimensions.get('window').width;
//ios will not make diff in ios but in android
//when we take screen=in ios it takes whole of status bar but in android it excludes status bar

  const styles = StyleSheet.create({
      container: {
          borderWidth: 4,
          borderColor: Colors.accent500,
          padding: deviceWidth < 380 ? 12:24,
          borderRadius: 8,
          margin: deviceWidth < 380 ? 12:24,
          alignItems: 'center',
          justifyContent: 'center'
      },
      numberText: {
          color: Colors.accent500,
          fontSize: deviceWidth < 380 ? 28:38,
        //   fontWeight: 'bold'
        fontFamily:'open-sans'
      }
  })
