import { ImageBackground, StyleSheet, Text, View,Image, SafeAreaView } from 'react-native';
import StartGameScreen from './Screens/StartGameScreen';
import React,{useState} from 'react'
import { LinearGradient } from 'expo-linear-gradient';
import GameScreen from "./Screens/GameScreen"
import Colors from './constants/Colors';
import GameOverScreen from './Screens/GameOverScreen'
import AppLoading from 'expo-app-loading';
import {useFonts} from 'expo-font';
import { StatusBar } from 'expo-status-bar';

export default function App() {
  const [userNumber, setUserNumber]= useState();
  const [gameIsOver, setGameIsOver]= useState(true);
  const [guessRounds, setGuessRounds] = useState(0)

let [fontsLoaded] = useFonts({
    'open-sans': require ('./assets/fonts/OpenSans-Regular.ttf'),
    'open-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf'),
  })

  if(!fontsLoaded){
    return <AppLoading/>
  }

  function pickedNumberHandler(pickerNumber){
    setUserNumber(pickerNumber);
    setGameIsOver(false);
  }
  function gameOverHandler( numberofRounds){
    setGameIsOver(true);
    setGuessRounds(numberofRounds)

  }
  function startNewGameHandler(){
    setUserNumber(null);
    setGuessRounds(0);
  }

  let screen = <StartGameScreen onPickNumber = {pickedNumberHandler}/>
  if (userNumber){
    screen=<GameScreen userNumber={userNumber} onGameOver={gameOverHandler}/>
  
  }
  if(gameIsOver && userNumber ){
    screen=<GameOverScreen
    userNumber={userNumber}
    roundsNumber={guessRounds}
    onStartNewGame={startNewGameHandler}/>
  }
  
  return (
    <>
   <StatusBar style='dark' />
    <LinearGradient colors={[Colors.primary500, Colors.accent500]} style={styles.container} >
       
      <ImageBackground 
      source={require('./assets/images/background.png')}
      resizeMode="cover"
      style={styles.background}
      imageStyle={styles.backgroundImage}
      >
        <SafeAreaView style={styles.container}>
        {screen}
        </SafeAreaView>
      
      </ImageBackground>
      
     </LinearGradient >
   </>
  );
}
const styles = StyleSheet.create({
  container:{
    flex:1,
  },

  backgroundImage:{
    opacity:0.15,
   
  },
  background:{
    position:"relative",
    width:"100%",
    height:"100%"
  }
});
