import React, {useState} from 'react'
import {TextInput, View, StyleSheet, Alert, Text} from 'react-native'
import Colors from '../constants/Colors';
import PrimaryButton from '../components/ui/PrimaryButton'
import Title from '../components/ui/Title';
import InstructionText from '../components/ui/InstructionText';
import {  useWindowDimensions, Dimensions, height } from 'react-native';
import { KeyboardAvoidingView } from 'react-native';
import { ScrollView } from 'react-native';
import Card from '../components/ui/Card';

function StartGameScreen( {onPickNumber}) {
  const [enteredNumber, setEnteredNumber] = useState('');

  const {width, height} = useWindowDimensions();
  
  function numberInputHandler (enteredText){
    setEnteredNumber(enteredText);
  }
  function resetInputHandler( ){
    setEnteredNumber('')
  }
  function confirmInputHandler(){
    const chosenNumber = parseInt(enteredNumber)
    if (isNaN (chosenNumber) || chosenNumber <= 0 || chosenNumber > 99){
      Alert.alert('Invilad number!', 'Number has to be anumber between 1 and 90',
      [{text: 'okay', style: 'destructive', onPress: resetInputHandler}]
      )
      return;
    }
    // console.log(chosenNumber)
    onPickNumber(chosenNumber)
  }
  const marginTopDistance = height<390 ? 30 : 100;
  return (
    <ScrollView style={styles.screen}>
    <KeyboardAvoidingView style={styles.screen} behavior="position">
    <View style={[styles.rootContainer,{marginTop:marginTopDistance}]}>
      <Title>Guess My Number</Title>
      
    <Card style={styles.inputContainer}>
      <InstructionText>Enter a number</InstructionText>
        <TextInput
         style={styles.numberInput}
         keyboardType='number-pad'
         maxLength={2}
         autoCapitalize='none'
         autoCorrect={false}
         value= {enteredNumber}
         onChangeText= {numberInputHandler}
         />
         <View style={styles.buttonsContainer}>
           <View style={styles.buttonContainer}>
              <PrimaryButton onPress={resetInputHandler}>Reset</PrimaryButton>
           </View>
           <View style={styles.buttonContainer}>
           <PrimaryButton onPress={confirmInputHandler}>Confirm</PrimaryButton>
           </View>
         </View>
    </Card>
    </View>
    </KeyboardAvoidingView>
    </ScrollView>
  )
}
export default StartGameScreen
//const deviceWidth = Dimensions.get('window').height;


const styles = StyleSheet.create({
  screen:{
    flex:1,
  },
  rootContainer:{
    flex:1,
    // marginTop:deviceWidth < 380 ? 30:100,
    alignItems:'center',
  },
  inputContainer: {
    justifyContent: 'center',
    alignItems:'center',
    marginTop: 100,
    marginHorizontal: 24,
    padding: 16,
    backgroundColor: Colors.primary500,
    borderRadius: 8,
    elevation: 4,
    shadowColor: 'black',
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 6,
    shadowOpacity: 0.25
  },
  numberInput: {
    height: 50,
    width: 50,
    fontSize: 32,
    borderBottomColor:Colors.accent500,
    borderBottomWidth: 2,
    color:Colors.accent500,
    marginVertical: 8,
    fontWeight: 'bold',
    textAlign: 'center'
},
buttonsContainer: {
    flexDirection: 'row'
},
buttonContainer: {
    flex: 1,
},
instructionText:{
  color: Colors.accent500,
  fontSize:24,
}
})
