import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <View style={styles.square1}>
      <Text>1</Text>
      </View> 
      <View style={styles.square2}>
      <Text>2</Text>
      </View> 
      <View style={styles.square3}>
      <Text>3</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: .25,
    flexDirection: 'row',
    backgroundColor: '#fff',    
    marginTop: 50,
    marginLeft: 50,

  },
  square1: {
    backgroundColor: 'red',
    width: 80,
    justifyContent: 'center',
    alignItems: 'center'
  },
  square2: {
    backgroundColor: 'blue',
    width: 150,
    justifyContent: 'center',
    alignItems: 'center'
  },
  square3: {
    backgroundColor: 'green',
    width: 8,
    justifyContent: 'center',
    alignItems: 'center'
  },
});
