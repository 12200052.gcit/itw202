import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import MessageQueue from 'react-native/Libraries/BatchedBridge/MessageQueue';
import MyComponent from './component/MyComponent';

export default function App() {
  return (
    <View style={styles.container}>
      <MyComponent/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
