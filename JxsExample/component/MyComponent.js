import React from "react";
import { View, StyleSheet, Text } from "react-native";

const MyComponent = () => {
const greeting = 'Sonam Wangmo';
const greeting1 = <Text>Jigme Wangmo</Text>
    return (
        <View>
            <Text style = {styles.textStyle}> This is a demo of JSX </Text>
            <Text>Hi There!!!{greeting}</Text>
            {greeting1}
        </View>)
};


export default MyComponent;

const styles = StyleSheet.create({
    textStyle: {
        fontSize: 24
    }
})