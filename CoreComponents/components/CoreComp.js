import React from "react";
import { StyleSheet, Text, View, ScrollView, Image, TextInput, Button } from "react-native";

export default function CoreComp(){
    return (
        <ScrollView>
            <Text>Some text</Text>
            <View>
                <Text>Some more text</Text>
                <Image
                  source={{
                      uri: 'https://picsum.photos/64/64',
                  }} />
            </View>
            <TextInput
              defaultValue="You can type here" />
            <Button
              onPress={() =>{
                  alert('You tapped the button');
              }}
              title="Press Me"/>
        </ScrollView>
    )
}
