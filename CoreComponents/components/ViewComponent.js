import React from "react";
import { View, Text } from "react-native";

export default function ViewComponent() {
    return (
        /*Base layout structure*/
        <View style={{ flex: 1}}>
            <View style={{ padding: 100, backgroundColor: 'pink'}}>
                <Text style={{ color: 'red'}}>Text with background color</Text>
            </View>
            <View style={{ margin: 16 }}/>
        </View>
    )
}