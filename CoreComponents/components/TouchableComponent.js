import React, {useState} from "react";
import { View, Text, StyleSheet, TouchableWithoutFeedback } from "react-native";
const TouchableNativeFeedbackExample = () => {
    const [count, setCount] = useState(0);
    return (
        <View style={{padding:100}}>
            <Text>You clicked {count} times</Text>
            <View>
            <TouchableWithoutFeedback style={styles.button}
                onPress={() => setCount(count + 1)}>
                <Text>Count</Text>
            </TouchableWithoutFeedback>
            </View>
        </View>
    );
};
const styles = StyleSheet.create({
    button:{
        alignItems: 'center',
        backgroundColor: '#DDDDDD',
        padding: 10
    }
});
export default TouchableNativeFeedbackExample;
