import React from "react";
import {View, Image, StyleSheet } from 'react-native';

const ImageComponent = () => {
    return (
        <View>
            <Image
            style={styles.logoContain}
            source={require('../assets/favicon.png')}
            />
            <Image
            style={styles.logoStretch}
            source={{uri: 'https://about.gitlab.com/images/topics/devops-lifecycle.png'}}//https://upload.wikimedia.org/wikipedia/commons/5/58/Sunset_2007-1.jpg
            />
        </View>
    )
}
export default ImageComponent

const styles = StyleSheet.create({
    logoContain: {
        width: 200, height: 100,
        resizeMode: 'contain',
    },
    logoStretch: {
        width: 200, height: 100,
        resizeMode: 'stretch',
    },
});