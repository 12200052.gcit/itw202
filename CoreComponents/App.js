import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import CoreComp from './components/CoreComp.js';
import ViewComponent from './components/ViewComponent.js';
import TextComponent from './components/TextComponent.js';
import ImageComponent from './components/ImageComponent.js';
import TextInputComponent from './components/TextInputComponent.js';
import TouchableReactNativeExample from './components/TouchableComponent.js';
export default function App() {
  return (
    <View style={styles.container}>
      <TouchableReactNativeExample></TouchableReactNativeExample>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
