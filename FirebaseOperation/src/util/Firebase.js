import { firebase } from '@firebase/app'

const CONFIG = {
  // apiKey: "AIzaSyAxmZ3xBFL7YRP8UF0XrAfed7hjSjGDTgs",
  // authDomain: "favoriteplace-750e2.firebaseapp.com",
  // databaseURL: "https://favoriteplace-750e2-default-rtdb.firebaseio.com",
  // projectId: "favoriteplace-750e2",
  // storageBucket: "favoriteplace-750e2.appspot.com",
  // messagingSenderId: "925753485241",
  // appId: "1:925753485241:web:aeffd942b416c820a43868"
  apiKey: "AIzaSyBQ_Es_SJri1xEFrT_7nWb3vqL4-QkpIrg",
  authDomain: "practice-27ab8.firebaseapp.com",
  projectId: "practice-27ab8",
  storageBucket: "practice-27ab8.appspot.com",
  messagingSenderId: "868048691812",
  appId: "1:868048691812:web:98b4787ec76f7169937571",
  measurementId: "G-KWQNDHBGT0",
}
firebase.initializeApp(CONFIG)

export default firebase;
